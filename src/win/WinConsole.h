/***************************************************************************
 *   Copyright (C) 2004-2021 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 2.0.1. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#ifndef WINCONSOLE_H
#define WINCONSOLE_H

#include <QProcess>


class WinConsole : public QObject
{

    Q_OBJECT

public:
    explicit WinConsole(QObject* parento = 0);
    ~WinConsole();


    int execute(const QString& program, const QStringList& arguments);


private slots:
    void output();


private:
    QProcess _process;
};

#endif
