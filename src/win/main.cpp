/***************************************************************************
 *   Copyright (C) 2004-2021 by Pere Constans
 *   constans@molspaces.com
 *   cb2Bib version 2.0.1. Licensed under the GNU GPL version 3.
 *   See the LICENSE file that comes with this distribution.
 ***************************************************************************/
#include "WinConsole.h"

#include <QCoreApplication>
#include <QFileInfo>

int main(int argc, char* argv[])
{
    if (argc < 1)
        return 1;
    QFileInfo fi(argv[0]);
    const QString exe(fi.absolutePath() + "/cb2bib.exe");
    QStringList arguments;
    for (int i = 1; i < argc; ++i)
        arguments.append(argv[i]);
    QCoreApplication a(argc, argv);
    WinConsole wc;
    const int code(wc.execute(exe, arguments));
    return code;
}
